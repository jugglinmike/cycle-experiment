import html from './html';

export default (sources) => {
	const domSource = sources.DOM;
	const props$ = sources.props;

	const newValue$ = domSource
		.select('input')
		.events('input')
		.map((event) => event.target.value);

	const state$ = props$
		.map((props) => {
			return newValue$
				.map((value) => ({
					label: props.label,
					unit: props.unit,
					min: props.min,
					value,
					max: props.max
				}))
				.startWith(props)
		})
		.flatten()
		.remember();

	const vdom$ = state$
		.map((state) => html`
			<div class="labeled-slider">
				<label>${state.label} ${state.value}${state.unit}</label>
				<input type="range"
					value=${state.value}
					min=${state.min}
					max=${state.max} />
			</div>
		`);

	return {
		DOM: vdom$,
		value: state$.map(({value}) => value)
	};
};
