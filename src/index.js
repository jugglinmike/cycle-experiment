import xs from 'xstream';
import {run} from '@cycle/run';
import isolate from '@cycle/isolate';
import {makeDOMDriver} from '@cycle/dom';
import html from './html';
import Slider from './slider';

function main(sources) {
	const weightProps$ = xs.of({
		label: 'Weight', unit: 'kg', min: 40, value: 70, max: 150
	});
	const heightProps$ = xs.of({
		label: 'Height', unit: 'cm', min: 140, value: 170, max: 210
	});

	const weightSlider = isolate(Slider, 'weight')
		({DOM: sources.DOM, props: weightProps$});
	const heightSlider = isolate(Slider, 'height')
		({DOM: sources.DOM, props: heightProps$});

	const weightVDom$ = weightSlider.DOM;
	const weightValue$ = weightSlider.value;

	const heightVDom$ = heightSlider.DOM;
	const heightValue$ = heightSlider.value;

	const bmi$ = xs.combine(weightValue$, heightValue$)
		.map(([weight, height]) => Math.round(weight / (height * 0.01) ** 2))
		.remember();

	const vdom$ = xs.combine(bmi$, weightVDom$, heightVDom$)
		.map(([bmi, weightVDom, heightVDom]) => html`
			<div>
				${weightVDom}
				${heightVDom}
				<h2>BMI is ${bmi}</h2>
			</div>
		`);

	return {DOM: vdom$};
}

const drivers = {
	DOM: makeDOMDriver('main')
};

run(main, drivers);
